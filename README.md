# 目标 task
    利用HMM求解POS tagging   solve POS tagging prob by HMM

# 数据 data
    带标注的数据traindata.txt  labeled data

# 基本步骤 process
    初始化词典及根据数据填充词典  initialize and fill in dic
    统计参数pi,A,B  get pi, A, B according to data
    维特比算法求解pos tagging definite viterbi algorithm to solve pos tagging prob
